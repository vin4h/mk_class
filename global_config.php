<?php

/*
 *  Locate Setting for GetText
 */

define("LOCALE", "pt-BR.utf8");
define("DOMAIN", "main");
define("__LOCALE_DIR__", "/var/www/html/mk_class/translation");

if( extension_loaded('gettext') === FALSE )
   exit("Extensio GetText Error or unistalled");

if(!putenv("LANG=" . LOCALE))
   exit('Falha no PutEnv 01');

if(!putenv("LANGUAGE=" . LOCALE))
   exit('Falha no PutEnv 02');

setlocale(LC_ALL, 'pt-BR.utf8');

bindtextdomain( DOMAIN, __LOCALE_DIR__ );

textdomain(DOMAIN);


?>
