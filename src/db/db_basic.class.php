<?php
require "/var/www/html/mk_class/src/db/conn_conf.php";


/*
 * Classe de operações básicas do POSTGRES com tratamentos de erros *
 * */


class PGsql
{
   private $conn_str = PGHOST . ' '. PGPORT.' '. ' '. PGUSER. ' '. PGPSWD;
   private $conn;
   private $rslt;
   private $num_rows;
   private $hashsql = "aded4fab587d35706c8ca649dbf643b2";
   
   public function 
   __construct() 
   {
      $this->conn = pg_connect($this->conn_str);
      if( pg_connection_status($this->conn) === PGSQL_CONNECTION_BAD )
         {
         //echo "Erro na inicializacao do Banco";
         return NULL;
         }
   }

   //---------------------------------------------------------------------------
   /*
    *  #WHERE
    */
   private function
   where($opt)
   {
      $conditions_opt = ['=', '<', '>', "<=", ">=", "!=",
                        "AND", "OR", "LIKE", "IN",
                        "IS NULL",  "IS NOT NULL",
                        "IS TRUE",  "IS NOT TRUE",
                        "IS FALSE", "IS NOT FALSE",
                        "%%"];
      if( !empty($opt) )
         {
         $conditions = "WHERE ";
         $opt_size = sizeof($opt);
         foreach( $opt as $var )
            {
            if( in_array( strtoupper($var[1]), $conditions_opt) )
               {              
               $conditions .= " {$var[0]} {$var[1]} '{$var[2]}'"; 
               }
            else if( in_array( strtoupper($var), $conditions_opt) )
               $conditions .= " $var";
            }
         
         return $conditions;
         }
      exit(1);
   }


   private function
   checkReplace($where, $sel)
   {
      if(!empty($sel) &&
        ($sel[1] === hash("sha256", sprintf("%s%s", $sel[0], $this->hashsql))) )
         {
         $where = str_replace("%%", "({$sel[0]})", "$where");
         return $where;         
         }
      return $where;
   }


   //---------------------------------------------------------------------------
   /*
    *  #CLOSE
    */
   public function
   close()
   {
      pg_close($this->conn);
   }
         
   public function
   query( $sql )
   {
      $final = [];

      /*Prepare the data result*/
      if( !($this->rslt = pg_query( $this->conn, $sql )) )
         return NULL;
      else
         while( $row = pg_fetch_assoc($this->rslt) )
            $final[] = $row;     

      /*Check rows number to use in Page Class*/
      $this->num_rows = pg_num_rows($this->rslt);

      return $final;
   }

   //---------------------------------------------------------------------------
   /*
    *  #SELECT                               
    *                                                                         
    * Atributos do metodo:
    *   $opt -> array  ["opt1", "opt2", ... , "opnN"];
    *        -> string "word"    OU    "word1, word2, word3"
    *
    *        
    *   $tables -> array  ["table1", "table2", ... , "tableN"];
    *           -> string "table"    OU    "table1, table2, table3"  
    *                                          
    *        
    *   array $opt = ( [["attr","cond","value"], "OPERATION", [A,C,V] ... "N"]) 
    *
    *   CONDITION = OR, AND, <=!>, IS NULL, etc.
    *
    *   ex.:
    *     select( 
    *           ["login.id AS Lid", "name.id"],
    *           "table1, table2"),
    *           ["Lid"=>""]
                );
    *                                                     
    */
   public function
   select( $col = null , $tables = null, $opt = null, $rslt = TRUE, $sel = NULL)
   { 
      $table;
      $where = '';
      $this->hashsql = 'aded4fab587d35706c8ca649dbf643b2';
      
      if( ($col && $tables) != NULL )
         {
         //Prepara as Colunas
         $col = implode(",", $col);
            
         $where = self::checkReplace(self::where($opt), $sel);
         

         //Prepara novas tabelas
         if( !empty($tables) )
            {
               if( is_array($tables) )
                  $table = implode( ",", $tables );
               else
                  $table = $tables;
            }


         $sql = "SELECT $col FROM $table $where";
         
         
         if($rslt)
         {
         echo "<pre><br>$sql<br></pre>";
            return( /*self::query("$sql;")*/ 0 );
          }  
         $this->hashsql = hash("sha256", sprintf("%s%s", $sql, $this->hashsql));
         return [$sql,$this->hashsql];
         }
      return FALSE;
   }
   

   //---------------------------------------------------------------------------
   /*
    *  #INSERT
    */
   public function
   insert($tables = NULL, $attr = NULL, $opt = NULL )
   {
      if( ($tables && $attr) != NULL ) 
         {
         if( !is_array($attr) )
            {
            $keys  = implode( "',"  , array_keys($attr) );
            $value = implode( "',", array_values($attr) );
            }
         $conditions = where($opt);
	 }
      $sql   = "INSERT INTO '{$this->table}' ($keys) VALUES ($value);";
      
      echo $sql;
      
      //return( self::query($sql) );
   }
   
   
   
   //---------------------------------------------------------------------------
   /*
    *  #ALTER
    */
   public function
   alter()
   {
   }
   
   //---------------------------------------------------------------------------
   /*
    *  #DELETE
    */   
   public function
   delete()
   {
   }

}



//function
//pgQuery( $query = '')

?>
