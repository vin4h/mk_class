<?php
/*  
 *  # Arquivo de configuracao exclusivo para trabalhar banco de dados
 *  
 *    Se adicionar novo banco, identificar apenas com duas siglas para o prefixo
 *  que representa o nome do banco.  Como PG = PostgreSQL e MY = Mysql, mantendo 
 *  os restante dos elementos identicos e padronizados.
 */


/*POSTGRESQL*/
define('PGHOST', 'host=127.0.0.1');
define('PGPORT', 'port=5432');
define('PGUSER', 'user=postgres');
define('PGPSWD', 'password=123');

/*MYSQL*/
//define('MYHOST', '');
//define('MYPORT', '');
//define('MYUSER', '');
//define('MYPSWD', '');
//define('MYDB', '');

 ?>
