<?php

require "utest_check/*"
require "utest_data.class.php";

define("BL", "<BR>");
define("BS", "<BR><BR>");

class UTest
{
   private $obj;
   private $class_name;
   private $count = 0;
   private $size;
   private $method_list;
   private $method_name;
   
   private $RFC;
   
   public function
   __construct( $obj )
   {  
      $this->obj = $obj;
            
      self::setClassName($this->obj);
      
      if( !self::getClassName() )
      {
         echo _("ALERTA - Não foi Possível iniciar este teste.");
         exit(1);
      }

      self::setMethodsList();
      self::setSize();
      
      printf(_("CLASSE::%s"), self::getClassName() );
      echo BL;
      printf( _("Teste Iniciado") );
      echo BL;
      printf( _(" + Numero de Metodos >> %d") , self::getSize() ); 
      echo BL,
           "--------------------------------------------------",
           BS;        
   }
   
   public function
   __destruct()
   {
      echo BL,
           "--------------------------------------------------",
           BL;
      printf( _("CLASSE::%s >> Teste Finalizado"), self::getClassName() );
      echo BS,
           _("RESULTADOS \/"),
           BS;
      
      for($this->count = 0 ; $this->count < $this->size ; $this->count++ )
      {
         printf( _(" + + + Metodo::%s"), self::getMethodName() ) ;
         echo BL;
      }
      
      echo "--------------------------------------------------",
           BS;     








   }
   public function
   check($test = '', $rslt = '')
   {
      $this->RFC = new ReflectionMethod( $this->obj, self::getMethodName() );

      
      if( $this->RFC->isPublic() )
      {
         echo "Metodo Publico <BR>";
      }
      else if( $this->RFC->isPrivate() )
      {
         echo "Metodo Privado <BR>";
      }
      else
      {
         echo "Metodo Protegido <BR>";
      }
   }
   
   
   
   
   
   
   
   //SETTERS
   protected function
   setClassName($obj)
   {
      $this->class_name = get_class($obj);
   }
   
   private function
   setSize()
   {
      $this->size = sizeof($this->method_list);
   }
   
   private function
   setMethodsList()
   {
      $this->method_list = get_class_methods($this->class_name);
   }
   

   
   //GETTERS
   private function
   getClassName()
   {
      return( $this->class_name );
   }
   
   public function
   getMethodName()
   {
      return( $this->method_name = $this->method_list[$this->count]);
   }
   
   private function
   getSize()
   {
      return($this->size);
   }

   public function
   init()
   {
      printf( _(" + + + METODO::%s >> Executando"), self::getMethodName() );
      echo BL;
   }
}


?>
